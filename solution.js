// https://www.codecademy.com/article/getting-user-input-in-node-jsconst readline = require('readline').createInterface({
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

readline.question('Type domain (e.g. google.com): ', domain => {

    /* SCRAPING */
    // https://javascript.plainenglish.io/scraping-a-wikipedia-page-with-nodejs-261655790882

    const pup = require('puppeteer');

    (async () => {
        const browser = await pup.launch();
        const page = await browser.newPage();

        await page.goto('https://en.wikipedia.org/wiki/' + domain);

        // https://pptr.dev/
        const resultsSelector = 'a.image';
        await page.waitForSelector(resultsSelector);

        // Extract the results from the page
        const links = await page.evaluate(resultsSelector => {
            return [...document.querySelectorAll(resultsSelector)].map(anchor => {
                return `${anchor.href}`;
            });
        }, resultsSelector);

        const logoURL = links[0];

        /* CSV FILE */
        // https://www.geeksforgeeks.org/node-js-fs-appendfile-function/

        const fs = require('fs');

        fs.appendFile("myWebsites.csv", `${domain}, ${logoURL}\n`, (err) => {
          if (err) {
            console.log(err);
          }
        });

        await browser.close()
    })();

    readline.close();
});
