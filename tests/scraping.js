const pup = require('puppeteer');
const fs = require('fs');


(async () => {
    const browser = await pup.launch();
    const page = await browser.newPage();

    await page.goto('https://en.wikipedia.org/wiki/facebook.com');

    /* SCRAPING */

    const resultsSelector = 'a.image';
    await page.waitForSelector(resultsSelector);

    // Extract the results from the page
    const links = await page.evaluate(resultsSelector => {
    return [...document.querySelectorAll(resultsSelector)].map(anchor => {
      return `${anchor.href}`;
    });
  }, resultsSelector);

    console.log(links);

    await browser.close()
})();
