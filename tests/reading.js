// https://www.codecademy.com/article/getting-user-input-in-node-jsconst readline = require('readline').createInterface({
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

readline.question('Type domain (e.g. google.com): ', domain => {
    console.log(`You typed: ${domain}`);
    readline.close();
});
